#!/usr/bin/python
"""
Convert G01 X0.000 Y1.000 Z.000 ... to
"Move J [*] X) .000 Y) 1.000 Z) 1.000 Z) W) 0 P) 0 R) 0
T) 0 Speed-25 Ad 15 As 10 Dd 20 Ds5 $F"
-i inputfile (gcode) -o outputfilename(pickle)
"""

import sys
import getopt
import pickle
import os.path


def main(argv):
    """
    Convert G01 x_value0.000 Y1.000 Z0.000 ... to
    "Move J [*] X) 1.000 Y) 1.000 Z) 1.000 Z) W) 0 P) 0 R) 0
    T) 0 Speed-25 Ad 15 As 10 Dd 20 Ds5 $F"
    -i inputfile (gcode) -o outputfilename(pickle)
    """

    inputfile = ''
    outputfile = ''

    try:
        opts, arg = getopt.getopt(argv, "hi:o:", ["ifile=", "ofile="])
    except getopt.GetoptError:
        print('convert_to_ar2.py -i <inputfile> -o <outputfile>')
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print('convert_to_ar2.py -i <inputfile> -o <outputfile>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg

    if inputfile == '':
        print('convert_to_ar2.py -i <inputfile> -o <outputfile>')
        sys.exit()

    openfile = open(inputfile, "r")
    text_2 = openfile.read()
    text_2_sp = text_2.split("\n")
    text_2_sp_converted = convert_gcode_to_christ_ar2(text_2_sp)

    if os.path.dirname(inputfile) != "":
        outputfile = os.path.dirname(inputfile) + '\\' + outputfile + '_ar2'
    else:
        pass
    pickle.dump(text_2_sp_converted, open(outputfile, "wb"))



def convert_gcode_to_christ_ar2(code):
    """
    receive list and convert only G1 for each command in oneline of block of GCode
    (i.e. [G01, x_value0.000, Y1.000 Z.000, ...]     to list of blocks of gcode christ AR2
    (i.e. [Move J ..., Move J ...]) and insert "Tab Number 1" and
    "##BEGINNING OF PROGRAM##" at the beginning of the return list
    """

    gcode = code
    for i in range(len(gcode)):
        converted = gcode
        type_of_command = gcode[i][:2]
        if type_of_command == "G1":
            command = gcode[i]

            x_index_begin = command.find("X")
            if command[x_index_begin:].find(" ") != -1:
                x_index_end = command[x_index_begin:].find(" ")  + x_index_begin
            else:
                x_index_end = len(command)

            y_index_begin = command.find("Y")
            if command[y_index_begin:].find(" ") != -1:
                y_index_end = command[y_index_begin:].find(" ")  + y_index_begin
            else:
                y_index_end = len(command)

            z_index_begin = command.find("Z")
            if command[z_index_begin:].find(" ") != -1:
                z_index_end = command[z_index_begin:].find(" ")  + z_index_begin
            else:
                z_index_end = len(command)

            # EIndexBegin = command.find("E")
            # if command[EIndexBegin:].find(" ") != -1 :
            #     EIndexEnd = command[EIndexBegin:].find(" ")  + EIndexBegin
            # else:
            #     EIndexEnd = len(command)

            # FIndexBegin = command.find("F")
            # if command[FIndexBegin:].find(" ") != -1 :
            #     FIndexEnd = command[FIndexBegin:].find(" ")  + FIndexBegin
            # else:
            #     FIndexEnd = len(command)

            # SIndexBegin = command.find("S")
            # if command[SIndexBegin:].find(" ") != -1 :
            #     SIndexEnd = command[SIndexBegin:].find(" ")  + SIndexBegin
            # else:
            #     SIndexEnd = len(command)
            if x_index_begin != -1:
                x_value = float(command[x_index_begin+1:x_index_end])
            else:
                x_value = ""
            if y_index_begin != -1:
                y_value = float(command[y_index_begin+1:y_index_end])
            else:
                y_value = ""
            if z_index_begin != -1:
                z_value = float(command[z_index_begin+1:z_index_end])
            else:
                z_value = ""
            # if EIndexBegin != -1:
            #     E = float (command[EIndexBegin+1:EIndexEnd])
            # else:
            #     E = ""
            # if FIndexBegin != -1:
            #     F = float (command[FIndexBegin+1:FIndexFnd])
            # else:
            #     F = ""
            # if SIndexBegin != -1:
            #     S = float (command[SIndexBegin+1:SIndexSnd])
            # else:
            #     S = ""

            converted[i] = "Move J [*] X) " + str(x_value) + " Y) " + str(y_value) + " Z) " + \
            str(z_value) + " W) 0 P) 0 R) 0 T) 0 Speed-25 Ad 15 As 10 Dd 20 Ds5 $F"
        else:
            converted[i] = gcode[i]
    converted.insert(0, "Tab Number 1")
    converted.insert(0, "##BEGINNING OF PROGRAM##")
    return converted



if __name__ == "__main__":
    main(sys.argv[1:])
