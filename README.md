# 3DPrinterToChrisAR2

convert_to_ar2.py -i \<inputfile\> -o \<outputfile\>

for using with Additional terminal commands for post processing in Simplify3D post processor.  
the output file will be created in same directory as gcode file.  
cmd /C python <.\convert_to_ar2.py> -i [output_filepath] -o [output_filename]  

### Note: replace <.\convert_to_ar2.py> to the convert_to_ar2.py location.

